#include <atomic>
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <numeric>
#include <mutex>
#include <future>
#include <algorithm>

template <typename T>
class DoubleBuffer
{
    private:
        std::atomic<T> current;
        std::atomic<T> expired;
        int counter {};
    public:
        explicit DoubleBuffer(T default_value=T()) {this->current.store(default_value);}
        T insertAndPop(T value);
        int useCount() {return counter;}
    private:
        void set(T value) {this->expired.store(value);}
        T get() {return this->current.exchange(this->expired.load());}
};

template <typename T>
T DoubleBuffer<T>::insertAndPop(T value)
{
    ++counter;
    set(value);
    return get();
}

std::mutex m;
DoubleBuffer<int> buffer;

template <typename Iterator>
int sum_with_buffer(Iterator begin, Iterator end)
{
    int sum {};
    for (auto it = begin; it != end; ++it)
    {
        std::lock_guard<std::mutex> lk{m};
        sum+=buffer.insertAndPop(*it);    
    }
    return sum;
}


template <typename Iterator>
int async_sum_with_buffer(Iterator begin, Iterator end)
{
    auto span=end-begin;
    auto middle=begin+span/2;
    
    if (span<5)
    {
        return std::accumulate(begin,end,0);
    }
    auto second_half_sum = std::async(std::launch::async, sum_with_buffer<Iterator>, middle, end);
    int first_half_sum = sum_with_buffer(begin, middle);
    return first_half_sum + second_half_sum.get();
}

int main()
{
    
    
    long seed {std::chrono::system_clock::now().time_since_epoch().count()};
    std::default_random_engine generator {seed};
    std::uniform_int_distribution<int> distribution(0, 10);
    
    
    std::vector<int> vec(100,0);
    std::generate(vec.begin(),vec.end(),[&](){
        return distribution(generator);
    });
    
    std::cout << async_sum_with_buffer(vec.begin(), vec.end()) << std::endl;

    return 0;
}
